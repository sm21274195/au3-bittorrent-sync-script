#comments-start
* Application Name:	ZipIt.au3
* Description:		App for zipping a large number of files.
* Parameter(s):     $1 - Complete path to text file with files to be zipped
*					$2 - Complete path to zip file that will be created (or handle if existant) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*
* Author(s):        sm21274195
* Notes:			none
#comments-end


#include "lib\Zip.au3"
#include "src\MsgBoxes.au3"

Global Const $ARRAY_SIZE = 4096
Global Const $ARCHIVE_DEFAULT_NAME = "Archive.zip"

Global $Zip, $fileListPath, $archiveName, $fZipList, $files[$ARRAY_SIZE]


; ============================
; Getting list of files to zip
; ============================

If $CmdLine[0] < 1 Then
	ErrorMessage("Application syntacsis:" & @CRLF & "ZipIt.exe <file-list-path> [archive-path]")			;; TODO: text formatting
	Exit
EndIf

$fileListPath = $CmdLine[1]

If FileExists($fileListPath) Then
	$fZipList = FileOpen($fileListPath, 0)
Else
	ErrorMessage("Couldn't find file: " & $fileListPath)
	Exit
EndIf

$archiveName = $ARCHIVE_DEFAULT_NAME
If $CmdLine[0] > 1 Then $archiveName = $CmdLine[2]

; ============================
; Parse files to get zipped
; ============================

$size = 0
While 1
	$str = FileReadLine($fZipList)

	If @error <> 0 Then ExitLoop
	If $size > $ARRAY_SIZE Then
		WarningMessage("Too many files, your list is being cropped")
		ExitLoop
	EndIf

	If StringLen($str) > 0 Then				;; TODO: add archive splitting by empty line

		$files[$size] = $str
		$size+=1
	EndIf
WEnd


FileClose($fZipList)
;_ArrayDisplay($files, "List of files")		; Debug

; ============================
; Creating Zip Archive
; ============================
; TODO if array is empty
If FileExists($archiveName) Then
	ErrorMessage("Archive " & $archiveName & " already exists. Delete it before new zip attempt.")
	Exit
EndIf

$Zip = _Zip_Create($archiveName)
If @error = 1 Then Exit

; ============================
; Adding files to archive
; ============================

For $i = 0 To $size - 1 Step 1
	If Not FileExists($files[$i]) Then
		DebugMessage("File " & $files[$i] & " couldn't be found.")
		ContinueLoop
	EndIf

	;_Zip_AddFolder($Zip,@desktopdir & "\DirTest", 4)			;Add a folder to the zip file (files/subfolders will be added)
	;_Zip_AddFolderContents($Zip, @DesktopDir & "\FTEST")		;Add a folder's content in the zip file

	$retValue = _Zip_AddFile($Zip, $files[$i])
Next


StatusMessage("Done!" & @CRLF & @CRLF & "Items set to move: " & $size & @CRLF & _
										"Items successfully moved: " & _Zip_Count($Zip) & @CRLF & _		; Count items in newly created .zip
										"==================" & @CRLF & _Zip_CountAll($Zip))				; Fetching details of newly created .zip


; ============================
; Functions "_Zip_Search" and "_Zip_List" Zip.au3 aren't work as expected.
; ============================

#comments-start
$search = _Zip_Search($Zip, "Screenshot_Test") ;Returns an array containing the search results
For $i = 0 to UBound($search) - 1   ; Print Each
    ConsoleWrite($search[$i] & @CRLF) ; Corresponding value
Next  									; In The Console

$list = _Zip_List($Zip) ;Returns an array containing all the files in the zip file
For $i = 0 to UBound($list) - 1
    ConsoleWrite($list[$i] & @LF)   ;Print Search Results in the console
Next
#comments-end


Exit