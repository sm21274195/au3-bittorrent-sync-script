#RequireAdmin

Func DebugMessage($mes)
	ConsoleWrite($mes & @CRLF)		; Warning: windows console doesn't work well with UTF-8
	; MsgBox(0, "Status", $mes)
EndFunc

Func StatusMessage($mes)
	DebugMessage($mes)

	MsgBox(0 + 64, "Status", $mes)	; $MB_ICONINFORMATION
EndFunc

Func WarningMessage($mes)
	DebugMessage($mes)

	Beep(600, 100)
	MsgBox(0 & 48, "Warning", $mes)	; $MB_ICONWARNING
EndFunc

Func ErrorMessage($mes)
	DebugMessage($mes)

	Beep(600, 100)
	MsgBox(0 & 16, "Error", $mes)	; $MB_ICONERROR
EndFunc